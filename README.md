# fluorite

Fluorite programming language

# What is this?

This is my practical work for the course TIES448 Kääntäjätekniikka. Unlike
instructed, this practical work was a solo project done by me, and no other
group members were involved. Sampsa Kiiskinen, however, offered some help, when
I faced obstacles.

# Short description

Fluorite is interpreted programming language written in Haskell. The language
is fairly limited and thus is not fit for any complicated tasks. It can do
basic arithmetic and I/O operations, like printing and getting user input (see
`plan.md` for syntax). Currently the interpreter doesn't give very useful error
messages, because it won't tell you where the error occured in the source file.
One would need to modify the AST to contain that information.

# Installation

## Tools needed

You will only need [**Haskell
Stack**](https://docs.haskellstack.org/en/stable/install_and_upgrade/) in order
to install Fluorite, since it will install the appropriate version of GHC and
all the packages needed.

## Installation process

Clone the Git repository and go into the project directory:
```
git clone https://gitlab.com/severij/fluorite
cd fluorite
```

Use stack to build the project:
```
stack build
```

Now you can run a fluorite source file
```
stack exec fluorite your_source_file
```
Many of the examples have the `.flu` file extension, but it's optional, and
right now the interpreter accepts files of any name. If you want, you can try
the examples found in the `examples/` directory. For example, if you want to
try the `factorial.flu` example, just type
```
stack exec fluorite examples/factorial.flu
```
in the project directory. You can also install Fluorite by running the command
```
stack install
```
In case of Linux (and probably macOS as well) the binary is saved in
`~/.local/bin/`, and this needs to be in the `PATH` environment variable if one
wishes to use the `fluorite` command without writing `~/.local/bin/fluorite`
every time the command is used. I'm not sure where this binary file is stored
in case of Windows, but Stack will probably prompt that directory after the
command. If the path is in the `PATH` environment variable, you can run
```
fluorite examples/factorial.flu
```
instead of
```
stack exec fluorite examples/factorial.flu
```
in the project directory. The `fluorite` command should work in any path.

# About the name

For some reason, a few programming languages are named after gemstones (Ruby,
Citrine, Crystal, Opal etc.). Fluorite follows this trend, although the
language itself is not very polished. ;)

