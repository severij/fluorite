# Plan for practical work 

- Project name: **Fluorite**
- Severi Jääskeläinen
- `severi.a.jaaskelainen@student.jyu.fi`

## Changes:

| **Date**   | **Modification**                                                                         |
| ---------- | ---------------------------------------------------------------------------------------- |
| 21.11.2018 | Plan written in Finnish                                                                  |
| 26.11.2018 | Plan translated to English                                                               |
| 12.2.2019  | Modified plan to include less features                                                   |
| 11.3.2019  | Renamed `print` to `put` and get_line to `get`. String concatenation happends with `++`. |
| 24.3.2019  | Changed single line comment to `#`                                                       |
| 25.3.2019  | Lots of small modifications, more examples etc.                                          |

## Description 

- Host language will be [Haskell](https://www.haskell.org/)
- Language will be interpreted
- Dynamically typed

### Tools and libraries

- [Stack](https://www.haskellstack.org/)
- Libraries:
  - [mtl](https://hackage.haskell.org/package/mtl)
  - [containers](https://hackage.haskell.org/package/containers)
  - [parser-combinators](https://hackage.haskell.org/package/parser-combinators)
  - [megaparsec](http://hackage.haskell.org/package/megaparsec)
  - [pretty](https://hackage.haskell.org/package/pretty)
  - [QuickCheck](http://hackage.haskell.org/package/QuickCheck)
- For testing: [QuickCheck](http://hackage.haskell.org/package/QuickCheck)
  - When testing the parser, I will use a method somewhat similar to the one described in [this blog post](https://lstephen.wordpress.com/2007/07/29/parsec-parser-testing-with-quickcheck/)
    The time for testing has been taken into account when planning the schedule.

### Version control

- Git + [GitLab](https://gitlab.com/)
- The project has a public [GitLab-repo](https://gitlab.com/severij/fluorite)

## Schedule  

| **Week** | **TODO**                                     |
| -------- | -------------------------------------------- |
| 7        | Parsing with megaparsec                      |
| 8        | Building the interpreter                     |
| 9        | Writing tests with QuickCheck                |
| 10       | Finishing touches                            |

**Goal for the number of course credits: 3**

## Description of the source language

- Dynamically typed
- Inspiration from high-level languages like
    - Ruby
    - Lua
    - Python

### About white space

The code doesn't need to be indented like in the following examples, but it
makes the code more readable. Keywords, however, must be separated from other
words with at least one whitespace character

### About statements and expressions

In the current version of the language, expressions can't exist without a
statement. For example, if you wish to display the result of a calculated
expression, you have to print it with `put`. Expressions themselves in code
won't do anything, and will result to a parsing error.

### Comments

```
# This is an inline comment 

---
This
is
a
multiline
comment
---

--- This will also work ---
```

### Basic operations

#### Arithmetic 

- Infix operators: `+`, `-`, `*`, `/` and `%`

Since the language is dynamically typed, the arithmetic operations will work in
case of integer and double values. Precedence of the operations will be the
same as in arithmetic operations in mathematics. 


##### Examples
```
3 + 7 / 4           # 5.25
(3 + 7) / 4         # 2.5
5 * 10 - 20         # 30.0
5 * (10 - 20)       # -50.0
10 - (3 + 7) / 4    # 7.5
2.5 * 3             # 7.5
1.2 / 0.4           # 3
7 % 3               # 1

```
#### Boolean algebra

- Values: `True` and `False`
- Operators: `and`, `or` and `not`

##### Examples
```
not True        # False
not False       # True
True and False  # False
True and True   # False
False or True   # True
```
### Comparison

- Operators: `==`, `!=`, `<`, `>`, `<=` and `>=`

##### Examples
```
3 == 3  # True
3 != 3  # False
2 < 6   # True
2 > 6   # False
1 >= 2  # False
1 <= 2  # True
```

### Strings

Strings are supported in the language. Strings must be surrounded with double
quotes. One can concatenate strings with `++` operator. If you wish to use
double quote in the string, escape the quote with backslash: `\"`.

##### Examples

```
"cat"
"fish
"cat" ++ "fish"  # "catfish"
```

### Variables 

Variable names may start only with a lowercase letter or underscore. Otherwise
variable names can contain lower and uppercase letter, numbers and underscores.

##### Examples

```
number = 1
is_even = 4 % 2 == 0
```

### Control structures

#### `if ... elif ... else`

```
if conditional then
   code block...
[elif conditional then
   code block...]
[else
   code block...]
end
```

##### Examples

Here the program prints `FizzBuzz` is x is divisible with both 3 and 5, `Fizz`
if x is divisible only with 3 and `Buzz` if x is divisible only with 5.
    Otherwise just the value of x is printed.

```
x = get "Give me a number: "
if x % 3 == 0 and x % 5 == 0 then
    put "FizzBuzz"
elif x % 3 == 0 then
    put "Fizz"
elif x % 5 == 0 then
    put "Buzz"
else
    put x
end
```

#### `while`

`while` will loop the code block inside it until the `conditional` is True:
```
while conditional do
   code block...
end
```

##### Examples

```
i = 0
while i < 10 do
    put i
    i = i + 1
end
```
The previous example will print integers from 0 to 9:
```
0
1
2
3
4
5
6
7
8
9
```

### IO

#### `put`

One can print any expression using the `put` command.

##### Examples

```
put 1 + 1
put "hello"
put True
put 3 / 2
```
The previous example will output
```
2
hello
True
1.5
```

#### `get`

One can get user input by using the `get` command. It takes one argument, which
is printed before the user input.

##### Examples

```
name = get "What is your name? "
put name
```
The previous example prints "What is you name? " and asks for user input. If we
type, for example, "Severi", the program outputs:
```
What is your name? Severi
Severi
```
