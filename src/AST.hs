module AST where

data Value = BooleanValue Bool
           | IntegerValue Integer
           | DoubleValue Double
           | StringValue String

instance Show Value where
    show (BooleanValue b) = show b
    show (IntegerValue i) = show i
    show (DoubleValue d) = show d
    show (StringValue s) = s

data Expression = Value Value
                | Variable String
                | Not Expression
                | And Expression  Expression
                | Or  Expression  Expression
                | Expression :+:  Expression
                | Expression :-:  Expression
                | Expression :*:  Expression
                | Expression :/:  Expression
                | Expression :%:  Expression
                | Expression :<:  Expression
                | Expression :>:  Expression
                | Expression :<=: Expression
                | Expression :>=: Expression
                | Expression :==: Expression
                | Expression :!=: Expression
                | Expression :++: Expression
                | Get Expression
                deriving (Show)

data Statement = If Expression Statement (Maybe Statement)
               | While Expression Statement
               | Assignment String Expression
               | Sequence [Statement]
               | Put Expression
               deriving (Show)



