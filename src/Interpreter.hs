module Interpreter where

import Data.Map (Map, empty, insert, lookup)
import Data.Foldable (forM_)
import Control.Monad (when)
import Control.Exception.Base (Exception, throw)
import Control.Applicative (liftA2)
import Control.Monad.State.Lazy (StateT, get, put, lift, liftIO)
import Text.Megaparsec (parseMaybe)
import Text.Megaparsec.Error (parseErrorPretty)
import Prelude hiding (lookup)
import AST
import Operation
import Type
import Parser (term)

type Context = Map String Value

newtype InterpreterException = UnboundVariable String

instance Show InterpreterException where
    show (UnboundVariable a) = "Unbound variable: " ++ a

instance Exception InterpreterException

type Interpreter = StateT Context IO

getVar :: String -> Interpreter Value
getVar v = do vars <- get
              case lookup v vars of
                  Just v'  -> return v'
                  Nothing -> throw $ UnboundVariable v

setVar :: String -> Value -> Interpreter ()
setVar vr vl = get >>= put . insert vr vl

evalBin :: (Value -> Value -> Value)
        -> Expression
        -> Expression
        -> Interpreter Value
evalBin op e1 e2 = liftA2 op (eval e1) (eval e2)

eval :: Expression -> Interpreter Value
eval (Value v)     = return v
eval (Variable v)  = getVar v
eval (Not e)       = notOp <$> eval e
eval (And e1   e2) = evalBin (boolBinOp (&&)) e1 e2
eval (Or  e1   e2) = evalBin (boolBinOp (||)) e1 e2
eval (e1  :+:  e2) = evalBin (arithOp   (+))  e1 e2
eval (e1  :-:  e2) = evalBin (arithOp   (-))  e1 e2
eval (e1  :*:  e2) = evalBin (arithOp   (*))  e1 e2
eval (e1  :/:  e2) = evalBin divOp            e1 e2
eval (e1  :%:  e2) = evalBin modOp            e1 e2
eval (e1  :<:  e2) = evalBin (compOp    (<))  e1 e2
eval (e1  :>:  e2) = evalBin (compOp    (>))  e1 e2
eval (e1  :<=: e2) = evalBin (compOp    (<=)) e1 e2
eval (e1  :>=: e2) = evalBin (compOp    (>=)) e1 e2
eval (e1  :==: e2) = evalBin (compOp    (==)) e1 e2
eval (e1  :!=: e2) = evalBin (compOp    (/=)) e1 e2
eval (e1  :++: e2) = evalBin catOp e1 e2
eval (Get e)   =
    do v <- eval e
       i <- liftIO $ putStr (show v) >> getLine
       case parseMaybe term i of
           Just e  -> eval e
           Nothing -> throw $ UnboundVariable "x"

exec :: Statement -> Interpreter ()
exec (Sequence [])     = return ()
exec (Sequence (x:xs)) = exec x >> exec (Sequence xs)
exec (Assignment v e)  = eval e >>= setVar v
exec (Put e)        = eval e >>= liftIO . print
exec (If e s m) =
    do v <- eval e
       if toBool v
           then exec s
           else forM_ m exec
exec (While e s) =
    do v <- eval e
       when (toBool v) (exec s >> exec (While e s))

