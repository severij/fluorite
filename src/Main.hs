module Main where

import Data.Map (empty)
import System.Environment (getArgs)
import System.IO (BufferMode(NoBuffering), hSetBuffering, stdout)

import Control.Monad.State.Lazy (runStateT)
import Text.Megaparsec (parse)
import Text.Megaparsec.Error (errorBundlePretty)

import Parser (program)
import Interpreter (exec)

main :: IO ()
main = do
    hSetBuffering stdout NoBuffering
    args <- getArgs
    case args of
        []       -> error "No input"
        [source] -> do
            contents <- readFile source
            case parse program source contents of
                Right v -> fmap fst (runStateT (exec v) empty)
                Left e  -> putStrLn (errorBundlePretty e)

        _        -> error "Only one file as an input is accepted!"
