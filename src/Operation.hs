{-# LANGUAGE RankNTypes #-}

module Operation where

import Data.Function (on)
import Control.Exception.Base (Exception, throw)
import Prelude hiding (toInteger)

import AST (Value(..))
import Type

newtype OperationError = DivisionByZero String deriving (Show)

instance Exception OperationError

-- Boolean binary operations ('and' and 'or').
boolBinOp :: (Bool -> Bool -> Bool) -> Value -> Value -> Value
boolBinOp op x y =
    case (boolBinType `on` typeOf) x y of
        BooleanType -> BooleanValue ((op `on` toBool) x y)
        _           -> undefined

-- Arithmetic operations (except division operation).
arithOp :: (forall a. Num a => a -> a -> a) -> Value -> Value -> Value
arithOp op x y =
    case (arithType `on` typeOf) x y of
        IntegerType -> IntegerValue ((op `on` toInteger) x y)
        DoubleType  -> DoubleValue ((op `on` toDouble) x y)
        _           -> undefined

-- Arithmetic division operation.
divOp :: Value -> Value -> Value
divOp x y =
    case (divType `on` typeOf) x y of
        DoubleType -> DoubleValue (((/) `on` toDouble) x y)
        _          -> undefined

-- Module operation.
modOp :: Value -> Value -> Value
modOp x y =
    case (modType `on` typeOf) x y of
        IntegerType ->
            case y of
                IntegerValue 0  -> throw (DivisionByZero "Division by zero")
                IntegerValue y' -> IntegerValue (mod (toInteger x) y')
        _           -> undefined

-- Boolean 'not' operation.
notOp :: Value -> Value
notOp x = case typeOf x of
          BooleanType -> BooleanValue $ Prelude.not $ toBool x
          _           -> undefined

-- Comparison operations (==, !=, <, >, <=, >=).
compOp :: (forall a. (Eq a, Ord a) => a -> a -> Bool)
       -> Value
       -> Value
       -> Value
compOp op x y = case (compType `on` typeOf) x y of
           BooleanType -> BooleanValue $ (op `on` toBool) x y
           IntegerType -> BooleanValue $ (op `on` toInteger) x y
           DoubleType  -> BooleanValue $ (op `on` toDouble) x y
           StringType  -> BooleanValue $ (op `on` toString) x y

-- String concatenation
catOp :: Value -> Value -> Value
catOp x y = case (catType `on` typeOf) x y of
          StringType -> StringValue $ ((++) `on` toString) x y
          _          -> undefined
