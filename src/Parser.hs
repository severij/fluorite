{-# LANGUAGE GADTs #-}

module Parser where

import Data.Void (Void)
import Data.Functor (($>))
import Data.Char (GeneralCategory (Space))
import Control.Monad.Combinators ((<|>), between, many, optional, sepEndBy1,
                                  some)
import Control.Monad.Combinators.Expr (makeExprParser, Operator(..))
import Text.Megaparsec (anySingle, notFollowedBy, takeWhileP, try, Parsec)
import Text.Megaparsec.Char (alphaNumChar, char, charCategory, eol, lowerChar,
                             space1, string, newline)

import qualified Text.Megaparsec.Char.Lexer as L
import AST

type Parser = Parsec Void String

spaceConsumer :: Parser ()
spaceConsumer = L.space space1 lineComment blockComment
  where
    lineComment  = L.skipLineComment "#"
    blockComment = L.skipBlockComment "---" "---"

lexeme :: Parser a -> Parser a
lexeme = L.lexeme spaceConsumer

integer :: Parser Integer
integer = lexeme L.decimal

double :: Parser Double
double = lexeme $ try L.float

string' :: Parser String
string' = lexeme $ do char '"'
                      blob ""
  where
    blob s = do b <- takeWhileP Nothing (\x -> x /= '"' && x /= '\\')
                c <- anySingle
                case c of
                    '"'  -> return (s ++ b)
                    '\\' -> anySingle >>= \x -> blob (s ++ b ++ [x])

equals :: Parser Char
equals = lexeme $ char '='

plus :: Parser Char
plus = lexeme $ char '+'

minus :: Parser Char
minus = lexeme $ char '-'

asterisk :: Parser Char
asterisk = lexeme $ char '*'

slash :: Parser Char
slash = lexeme $ char '/'

percent :: Parser Char
percent = lexeme $ char '%'

leftParen :: Parser Char
leftParen = lexeme $ char '('

rightParen :: Parser Char
rightParen = lexeme $ char ')'

underscore :: Parser Char
underscore = lexeme $ char '_'

cat :: Parser String
cat = lexeme $ string "++"

lt :: Parser String
lt = lexeme $ string "<"

gt :: Parser String
gt = lexeme $ string ">"

lte :: Parser String
lte = lexeme $ string "<="

gte :: Parser String
gte = lexeme $ string ">="

eq :: Parser String
eq = lexeme $ string "=="

neq :: Parser String
neq = lexeme $ string "!="

semicolon :: Parser Char
semicolon = lexeme $ char ';'

reservedWord :: String -> Parser ()
reservedWord w = lexeme $ try $ string w *> notFollowedBy alphaNumChar

reservedWords :: [String]
reservedWords = ["and", "do", "elif", "else", "end", "False", "get", "if",
                 "or", "put", "then", "True", "while"]

identifier :: Parser String
identifier = lexeme $ try $ p >>= check
  where
    p = (:) <$> (lowerChar <|> underscore)
            <*> many (alphaNumChar <|> underscore)
    check x = if x `elem` reservedWords
                  then fail $ "Keyword " ++ x ++ " cannot be " ++
                              "used as an identifier"
                  else return x

true :: Parser Bool
true = reservedWord "True" $> True

false :: Parser Bool
false = reservedWord "False" $> False

get :: Parser ()
get = reservedWord "get"

put :: Parser ()
put = reservedWord "put"

not' :: Parser ()
not' = reservedWord "not"

and' :: Parser ()
and' = reservedWord "and"

or' :: Parser ()
or' = reservedWord "or"

if' :: Parser ()
if' = reservedWord "if"

elif :: Parser ()
elif = reservedWord "elif"

else' :: Parser ()
else' = reservedWord "else"

then' :: Parser ()
then' = reservedWord "then"

while :: Parser ()
while = reservedWord "while"

do' :: Parser ()
do' = reservedWord "do"

end :: Parser ()
end = reservedWord "end"

operators :: [[Operator Parser Expression]]
operators = [ [ InfixL ((:++:) <$ cat) ]
            , [ InfixL ((:*:) <$ asterisk)
              , InfixL ((:/:) <$ slash)
              ]
            , [ InfixL ((:+:) <$ plus)
              , InfixL ((:-:) <$ minus)
              ]
            , [ InfixL ((:%:)  <$ percent) ]
            , [ InfixL ((:<=:)  <$ lte)
              , InfixL ((:>=:)  <$ gte)
              , InfixL ((:<:) <$ lt)
              , InfixL ((:>:) <$ gt)
              , InfixL ((:==:) <$ eq)
              , InfixL ((:!=:) <$ neq)
              ]
            , [ Prefix (Not <$ not') ]
            , [ InfixL (And <$ and')
              , InfixL (Or  <$ or')
              ]
            ]

parens :: Parser a -> Parser a
parens = between leftParen rightParen

term :: Parser Expression
term =  parens expression
    <|> Value . DoubleValue  <$> double
    <|> Value . IntegerValue <$> integer
    <|> Value . BooleanValue <$> (true <|> false)
    <|> Value . StringValue  <$> string'
    <|> Variable  <$> identifier

expression :: Parser Expression
expression =  parens expression
          <|> makeExprParser term operators
          <|> Get <$> (get *> expression)
          <|> term

sequence' :: Parser Statement
sequence' = Sequence <$> some statement

whileStatement :: Parser Statement
whileStatement = do while
                    e <- expression
                    do'
                    s <- sequence'
                    end
                    return $ While e s

assignment :: Parser Statement
assignment = try $ do v <- identifier
                      equals
                      Assignment v <$> expression

putStatement :: Parser Statement
putStatement = put *> (Put <$> expression)

ifStatement :: Parser Statement
ifStatement = do if'
                 e <- expression
                 then'
                 s <- sequence'
                 a <- optional (elifBlock <|> elseBlock)
                 end
                 return $ If e s a
  where 
    elseBlock = else' *> sequence'
    elifBlock = do elif
                   e <- expression
                   then'
                   s <- sequence'
                   a <- elifBlock <|> elseBlock
                   return $ If e s (Just a)

statement :: Parser Statement
statement =  ifStatement
         <|> whileStatement
         <|> putStatement
         <|> assignment

program :: Parser Statement
program = spaceConsumer *> sequence'
