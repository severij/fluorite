module Type where

import Control.Exception.Base (Exception, throw)
import AST (Value(..))

-- These are all the types we have in the language.
data Type = BooleanType
          | IntegerType
          | DoubleType
          | StringType
          deriving (Bounded, Enum, Ord, Eq)

instance Show Type where
    show BooleanType = "Boolean"
    show IntegerType = "Integer"
    show DoubleType  = "Double"
    show StringType  = "String"

-- The first parameter of UnexceptedType is list of types that would have been
-- valid (expected). The second parameter is the actual type of the value
-- provied.
data TypeError = UnexpectedType [Type] Type

instance Show TypeError where
    show (UnexpectedType e a) = "Unexpected type: Expected one of " ++
                                show e ++ ", but got " ++ show a

instance Exception TypeError

-- We need a way of telling the type of a value.
typeOf :: Value -> Type
typeOf (BooleanValue _) = BooleanType
typeOf (IntegerValue _) = IntegerType
typeOf (DoubleValue  _) = DoubleType
typeOf (StringValue  _) = StringType

-- Function for converting Value to Bool.
toBool :: Value -> Bool
toBool (BooleanValue b) = b
toBool (IntegerValue i) = i /= 0
toBool (DoubleValue d)  = d /= 0
toBool (StringValue s)  = not (null s)

-- Function for converting Value to Integer.
toInteger :: Value -> Integer
toInteger (BooleanValue True)  = 1
toInteger (BooleanValue False) = 0
toInteger (IntegerValue i)     = i
toInteger (DoubleValue d)      = round d
toInteger (StringValue s)      = read s

-- Function for converting Value to Double.
toDouble :: Value -> Double
toDouble (BooleanValue True)  = 1.0
toDouble (BooleanValue False) = 0.0
toDouble (IntegerValue i)     = fromIntegral i
toDouble (DoubleValue d)      = d
toDouble (StringValue s)      = read s

-- Function for converting Value to String.
toString :: Value -> String
toString = show

-- A helper function for checking binary functions, which takes operands that
-- have the same type.
same :: Type -> Type -> Type -> Type
same t x y
    | t == x && t == y = t
    | t == x           = throw (UnexpectedType [t] y)
    | otherwise        = throw (UnexpectedType [t] x)

-- Boolean binary operations, like 'and' and 'or', should be OK if both
-- operands are of Boolean type.
boolBinType = same BooleanType

-- Division operation should only accept Double types.
divType = same DoubleType

-- Modulo operation should only accept Integer types.
modType = same IntegerType

-- String concatenation operation should only accept String types.
catType :: Type -> Type -> Type
catType _ _ = StringType

-- Error raised when types don't match for arithmetic operations.
arithTypeError :: Type
arithTypeError = throw (UnexpectedType [IntegerType, DoubleType] StringType)

-- Check type of arithmetic operations
arithType :: Type -> Type -> Type
arithType StringType  _           = arithTypeError 
arithType _           StringType  = arithTypeError
arithType BooleanType _           = arithTypeError
arithType _           BooleanType = arithTypeError
arithType x           y           = max x y

-- Check type of boolean 'not' operation.
notType :: Type -> Type
notType BooleanType = BooleanType
notType x           = throw $ UnexpectedType [BooleanType] x

-- Check type of comparison operation.
compType :: Type -> Type -> Type
compType x y
    | x == y    = x
    | otherwise = throw (UnexpectedType [x] y)
